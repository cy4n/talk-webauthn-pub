# passkeys, FIDO2 - slide dump repo 😅

# this repo contains multiple slidedecks from various semi-similar talks on passkeys

Slides for my Chaos Communication Camp 2023 presentation on FIDO2, Webauthn and passkeys.

https://gitlab.com/cy4n/talk-webauthn-pub/-/blob/main/webauthn_CCCamp23.pdf


you can view the recording (in english) at 

https://media.ccc.de/v/camp2023-57174-fido2


# also now included: slides for my passkeys-Talk at Google Developer Group Karlsruhe DevFest 2023 

https://gitlab.com/cy4n/talk-webauthn-pub/-/blob/main/webauthn_devfest23.pdf


# aaand GPN'24:
https://gitlab.com/cy4n/talk-webauthn-pub/-/raw/main/passkeys_2024_GPN.pdf?ref_type=heads&inline=false


# and IT-Tage 2024 Frankfurt:

